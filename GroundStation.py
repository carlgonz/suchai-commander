#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#                          SERIAL COMMANDER
#                A simple serial interface to send commands
#
#      Copyright 2013-2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import time
import shutil
import datetime
import threading
import serial

from PyQt5.Qt import *
from PyQt5 import QtWidgets
from PyQt5 import QtCore

from csp_zmq.zmqnode import CspZmqNode, CspHeader

from forms.Console import Ui_MainWindow
from forms.EditCommandDialog import Ui_DialogEditCommandList

# os.path.expanduser("C:\\Users\\$USERNAME\\AppData\\Roaming\\Python\\share\\serialcommander")]
current_path = os.path.dirname(os.path.abspath(__file__))
config_path = os.path.join(current_path, "../share/serialcommander")
local_config_path = os.path.join(os.path.expanduser("~"), ".config", "serialcommander")
print(config_path)

if not os.path.exists(local_config_path):
    print("No local config path. Creating")
    os.makedirs(local_config_path)

    if os.path.exists(config_path):
        shutil.copy(os.path.join(config_path, "config.json"), local_config_path)
        shutil.copy(os.path.join(config_path, "cmd_list.txt"), local_config_path)
        shutil.copy(os.path.join(config_path, "version.txt"), local_config_path)


class GroundStation(QtWidgets.QMainWindow):
    """
    Clase principal, aca se crea y configura la ventana. Tambien contiene
    los slots y conexiones con otras senales
    """
    # Signals
    _new_char = pyqtSignal(type(""))  # Se lee un nuevo char

    def __init__(self):
        """
        Se inicializan la ventana llamando al metodo setupUi y se realizan
        las conexiones signals-slots
        """

        QtWidgets.QMainWindow.__init__(self)

        # Variables de instancia
        # self.SerialComm = serial.Serial()
        self.node = None  # CspZmqNode(12)
        self.node_id = 13
        self.alive = False
        self.receiver_thread = None
        self.commands_file = os.path.join(local_config_path, "cmd_list.txt")
        self.commands_list = []
        self.history = []
        self.history_cnt = 0
        self.history_max = 1000
        self.timestamp = False
        self.put_timestamp = True
        self.auto_scroll = True

        # Configurar ventana
        self.ventana = Ui_MainWindow()
        self.ventana.setupUi(self)
        self.setup_comm()
        self.setup_send()
        self.setup_actions()

        # Load version
        self.version = "0.0.0"
        try:
            with open(os.path.join(local_config_path, "version.txt")) as ver_file:
                self.version = ver_file.readline()
        except IOError:
            print("Version file not found")
            pass

    def about(self):
        msg = "Version {}".format(self.version)
        QtWidgets.QMessageBox.information(self, "About", msg, QtWidgets.QMessageBox.Ok)
        return

    def setup_comm(self):
        """
        Configura el combo-box conexiones completando puertos
        y baudrates disponibles
        """

        # Conexiones
        self.ventana.pushButtonOpenPort.clicked.connect(self.open_port)
        self.ventana.pushButtonClosePort.clicked.connect(self.close_port)
        # self.ventana.pushButtonPortRefresh.clicked.connect(self.refresh_port)

        # Se agregan los puertos disponibles
        # self.refresh_port()

    def setup_send(self):
        """
        Configura la ventana de seleccion y envio de comandos
        """
        # Leer el archivo con la lista de comandos disponibles
        try:
            cmd_file = open(self.commands_file, 'r')
            self.commands_list = cmd_file.readlines()
            cmd_file.close()
        except OSError:
            cmd_file = open(self.commands_file, 'w')
            cmd_file.close()

        # Agregar los comandos a la lista
        for i in range(len(self.commands_list)):
            self.commands_list[i] = self.commands_list[i][:-1]

        self.ventana.listWidgetCommand.addItems(self.commands_list)

        # Conexiones
        self.ventana.listWidgetCommand.itemDoubleClicked.connect(self.command_clicked)
        self.ventana.pushButtonSend.clicked.connect(self.send_msg)
        self.ventana.checkBoxTimestamp.toggled.connect(self.timestamp_toggle)
        self.ventana.checkBoxScroll.toggled.connect(self.scroll_toggle)

    def timestamp_toggle(self, value):
        """
        Slot que intercambia entre agregar o no la marca de tiempo
        """
        self.timestamp = value

    def scroll_toggle(self, value):
        """
        Toggle auto scroll variable
        """
        self.auto_scroll = value

    def setup_actions(self):
        """
        Configura los menus de la barra de herramientas
        """
        self.ventana.actionGuardar.triggered.connect(self.save_log)
        self.ventana.actionAgregar_comando.triggered.connect(self.add_cmd)
        self.ventana.actionAcerca_de.triggered.connect(self.about)
        self._new_char.connect(self.write_terminal)

    def add_cmd(self):
        """
        Edita la lista de comandos
        """
        dialog = EditCommandDialog(self, self.commands_list)
        self.commands_list = dialog.run_tool()

        self.ventana.listWidgetCommand.clear()
        self.ventana.listWidgetCommand.addItems(self.commands_list)

    def write_terminal(self, text, *args, **kwargs):
        """
        Escribe el texto en el widget de lectura, configura el
        cursor hasta el final de cocumento y agrega marca de tiempo
        """
        # text = text.replace('\r', '')
        text = text.decode('ascii', errors='replace')
        if self.timestamp:
            ts = "[{}] ".format(datetime.datetime.now().isoformat(' '))
            if self.put_timestamp:
                text = ts + text
            if text[-1] == "\n":
                self.put_timestamp = True
            text = text[:-1].replace("\n", "\n" + ts) + text[-1]

        prev_cursor = self.ventana.textEditTerminal.textCursor()
        prev_scrollbar_value = self.ventana.textEditTerminal.verticalScrollBar().value()

        self.ventana.textEditTerminal.moveCursor(QTextCursor.End)
        self.ventana.textEditTerminal.insertPlainText(text)
        self.ventana.textEditTerminal.moveCursor(QTextCursor.End)

        if not self.auto_scroll:
            self.ventana.textEditTerminal.setTextCursor(prev_cursor)
            self.ventana.textEditTerminal.verticalScrollBar().setValue(prev_scrollbar_value)

    def command_clicked(self, item):
        """
        Mueve un comando de la lista, a la salida de texto
        """
        self.ventana.lineEditSend.setText(item.text())

    def open_port(self):
        """
        Abre el puerto serial indicado en la GUI
        """
        self.ventana.pushButtonClosePort.setEnabled(True)
        self.ventana.pushButtonOpenPort.setEnabled(False)
        self.ventana.pushButtonSend.setEnabled(True)

        # puerto = self.ventana.comboBoxPorts.currentText()
        # baudrate = self.ventana.comboBoxBaudrate.currentText()
        ip = self.ventana.IpLineEdit.text()
        self.node = CspZmqNode(self.node_id, ip, writer=True)
        self.node.read_message = self.write_terminal
        self.node.start()
        self.alive = True

        # self.SerialComm.port = str(puerto)
        # self.SerialComm.baudrate = int(baudrate)
        # self.SerialComm.timeout = 0.1  # [s]
        # self.SerialComm.interCharTimeout = 0.01  # [s]

        # try:
            # self.SerialComm.open()
            # self.alive = True
            # self.receiver_thread = threading.Thread(target=self.reader)
            # self.receiver_thread.setDaemon(True)
            # self.receiver_thread.start()
        # except Exception as e:
        #     QMessageBox.critical(self, 'Error', 'Error al abrir el puerto serial\n' + str(e))
        #     self.close_port()
        #
        print(self.node)

    def close_port(self):
        """
        Cierra la comunicacion serial actual
        """
        self.ventana.pushButtonClosePort.setEnabled(False)
        self.ventana.pushButtonOpenPort.setEnabled(True)
        # self.ventana.pushButtonSend.setEnabled(False)
        self.alive = False
        if self.receiver_thread is not None:
            self.receiver_thread.join()
        # self.SerialComm.close()
        self.node.stop()
        self.node = None

    def refresh_port(self):
        """
        Refresh list of available ports and baudrates
        """
        pass
        # Add available ports
        # comm_ports = listports()
        # comm_ports.reverse()
        # port_names = [port[0] for port in comm_ports]
        # self.ventana.comboBoxPorts.clear()
        # self.ventana.comboBoxPorts.addItems(port_names)

        # Add available baudrates
        # baudrates = str(serial.Serial.BAUDRATES)[1:-1].split(',')
        # self.ventana.comboBoxBaudrate.clear()
        # self.ventana.comboBoxBaudrate.addItems(baudrates)

    def send_msg(self):
        """
        Send text using the serial port
        """
        if not self.alive:
            self.ventana.lineEditSend.clear()
            return

        msg = str(self.ventana.lineEditSend.text())
        self.add_history(msg)

        # Add LF, CR and/or Echo
        if self.ventana.checkBoxEcho.isChecked():
            self._new_char.emit(msg)
        if self.ventana.checkBoxLF.isChecked():
            msg += '\n'
        if self.ventana.checkBoxCR.isChecked():
            msg += '\r'

        self.ventana.lineEditSend.clear()
        self.history_cnt = 0
        # self.SerialComm.write(msg.encode("utf8"))
        hdr = CspHeader(src_node=int(self.node_id), dst_node=int(10), dst_port=int(12))
        self.node.send_message(msg, hdr)

    def save_log(self):
        """
        Guarda el contenido de la ventana de log a un archivo
        """
        # Get filename dialog
        doc_file, _ = QFileDialog.getSaveFileName(self, "Guardar archivo", QDir.currentPath(),
                                                  "Archivos de texto (*.txt);;All files (*.*)")
        if doc_file:
            doc_file = str(doc_file)
            # Save file using python
            with open(doc_file, 'w') as text_file:
                text_file.write(self.ventana.textEditTerminal.toPlainText())

    def reader(self):
        """
        Lee en un thread los datos seriales y los muestra en pantalla
        """
        pass
        # while self.alive:
        #     try:
        #         to_read = # self.SerialComm.inWaiting()
        #         if to_read:
        #             data = # self.SerialComm.read(to_read)
        #             if len(data) and (not data == '\r'):
        #                 self._new_char.emit(data.decode("utf8"))
        #         else:
        #             time.sleep(0.1)
        #
        #     except UnicodeDecodeError:
        #         pass
        #
        #     except serial.SerialException:
        #         self.alive = False
        #         raise

    def add_history(self, line):
        """
        Agrega una nueva linea al historial. Elimina entradas antiguas si supera
        cierta cantidad de mensajes.
        """
        try:
            if not (line == self.history[-1]):
                self.history.append(line)
        except IndexError:
            self.history.append(line)

        if len(self.history) > self.history_max:
            self.history.pop(0)

    def get_history(self, index):
        """
        Retorna el elemendo numero index del historial
        """
        if 0 < index <= len(self.history):
            return self.history[-index]
        else:
            return ''

    def history_send(self):
        """
        Agrega una linea del historial para ser enviada
        """
        if self.history_cnt > len(self.history):
            self.history_cnt = len(self.history)
        elif self.history_cnt < 0:
            self.history_cnt = 0
        else:
            text = self.get_history(self.history_cnt)
            self.ventana.lineEditSend.setText(text)

    def closeEvent(self, event):
        """
        Cierra la aplicacion correctamente. Cerrar los puertos, detener thread
        y guardar la lista de comandos creada
        """
        if self.alive:
            self.close_port()
        file_cmd = open(self.commands_file, 'w')
        for line in self.commands_list:
            file_cmd.write(line + '\n')
        file_cmd.close()
        event.accept()

    def keyPressEvent(self, event):
        """
        Maneja eventos asociados a teclas presionadas
        """
        if event.key() == QtCore.Qt.Key_Up:
            if self.ventana.lineEditSend.hasFocus():
                self.history_cnt += 1
                self.history_send()

        if event.key() == QtCore.Qt.Key_Down:
            if self.ventana.lineEditSend.hasFocus():
                self.history_cnt -= 1
                self.history_send()

        event.accept()


class EditCommandDialog(QtWidgets.QDialog):
    """
    Herramienta para edicion de comandos
    """

    def __init__(self, parent=None, cmd_list=None):

        QtWidgets.QDialog.__init__(self, parent)

        self.ventana = Ui_DialogEditCommandList()
        self.ventana.setupUi(self)

        self.cmd_list = cmd_list if cmd_list is not None else []
        for cmd in self.cmd_list:
            self.ventana.plainTextEditCommand.appendPlainText(cmd)

        # Conexiones
        self.ventana.pushButtonAdd.clicked.connect(self.add_item)

    def run_tool(self):
        """
        Abre el dialogo para que el usuario la lista. Al cerrar, recupera
        los cambios y retorna la nueva lista
        """
        ret = super(EditCommandDialog, self).exec_()

        if ret:
            self.cmd_list = self.ventana.plainTextEditCommand.toPlainText().split('\n')

        return self.cmd_list

    def delete_item(self):
        """
        Borra los item seleccionados
        """
        pass

    def add_item(self):
        """
        Agrega un nuevo item 
        """
        cmd = self.ventana.lineEditAdd.text()
        self.ventana.plainTextEditCommand.appendPlainText(cmd)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    gui = GroundStation()
    gui.show()
    sys.exit(app.exec_())
