SerialCommander
===============

A simple serial interface to send commands

![](forms/sc.png)

## Install

### Ubuntu and derivatives
- Install dependencies

    ```
    sudo apt-get install python3-pyqt5 python3-pip
    ```
  
- Build pip package (or download Serial Commander [latest release](https://gitlab.com/carlgonz/SerialCommander/-/releases))

    ```
    git clone https://gitlab.com/carlgonz/SerialCommander.git
    cd SerialCommander
    make
    cd dist
    ```
    
- Install with pip
    
    ```
    sudo pip install serialcommander-X.X.X.tar.gz
    ```
  
- Execute from console
  
    ```  
    SerialCommander.py
    ```
    
### Windows
- Install miniconda or anaconda
- Install dependencies

    ```
    conda install -c anaconda pyqt
    ```

- Download Serial Commander [latest release](https://gitlab.com/carlgonz/SerialCommander/-/releases)
- Install with pip 
    
    ```
    pip install serialcommander-X.X.X.tar.gz
    ``` 
- Open an anaconda prompt (or power shell)
- Open the anaconda script folders an launch SerialCommander

    ```
    cd C:\Users\$USER\AppData\Local\Continuum\miniconda3\Scripts
    python .\SerialCommander.py
    ```

## Contact

Please use the issue tracker for questions, suggestion and error reports.

![Twitter Follow](https://img.shields.io/twitter/follow/cegonzalezspace?label=Follow%20me&style=social)